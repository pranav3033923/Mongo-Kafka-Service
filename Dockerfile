FROM openjdk:17
VOLUME /tmp
EXPOSE 8070
ARG JAR_FILE=target/kakfamongoservice-1.0-SNAPSHOT-jar-with-dependencies.jar
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]