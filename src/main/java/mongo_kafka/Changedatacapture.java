package mongo_kafka;

import com.mongodb.BasicDBObject;
import com.mongodb.ConnectionString;
import com.mongodb.DBObject;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.client.model.changestream.UpdateDescription;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.print.Doc;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import static com.mongodb.client.model.Filters.eq;

public class Changedatacapture {
    private static final Logger logger = LogManager.getLogger(Changedatacapture.class);
    private static final String SECRETS_FILE = "secrets.properties";
    private static final String CLIENT_FILE = "client.properties";
    private static final String MONGODBCONNECTIONSTRING = "mongo.url";
    private static final String MONGODBDATABASE = "mongo.db";
    private static final String MONGODBCOLLECTION = "mongo.collection";
    private static final String KAFKATOPIC = "kafka.topic";
    private static String MONGODB_CONNECTION_STRING="";
    private static String MONGODB_DATABASE="";
    private static String MONGODB_COLLECTION="";
    private static String KAFKA_TOPIC="";

    public static void main(String[] args) throws IOException {
        Properties properties = loadProperties(SECRETS_FILE);

        // Set up MongoDB connection
        MONGODB_CONNECTION_STRING=properties.getProperty(MONGODBCONNECTIONSTRING);
        MONGODB_DATABASE=properties.getProperty(MONGODBDATABASE);
        MONGODB_COLLECTION=properties.getProperty(MONGODBCOLLECTION);


        // Set up Kafka producer
        Properties props = loadProperties(CLIENT_FILE);
        KAFKA_TOPIC=properties.getProperty(KAFKATOPIC);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        KafkaProducer<String, String> producer = new KafkaProducer<>(props);



        // Setting up MongoDB
        ConnectionString connectionString = new ConnectionString(MONGODB_CONNECTION_STRING);
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .build();
        try (var mongoClient = MongoClients.create(mongoClientSettings)) {

            MongoDatabase database = mongoClient.getDatabase(MONGODB_DATABASE);
            MongoCollection<Document> collection = database.getCollection(MONGODB_COLLECTION);




            // Create change stream cursor
            try (MongoCursor<ChangeStreamDocument<Document>> cursor = collection.watch().iterator()) {
                while (cursor.hasNext()) {
                    ChangeStreamDocument<Document> document = cursor.next();
                    ThreadContext.put("statusCode","200");
                    logger.info("Service MONGO - KAFKA Success:1.0 - Change Stream Doc[--] Captured from MongoDB");
                    ThreadContext.clearMap();

                    String operationType = document.getOperationType().getValue();

                    Document newDoc= Document.parse(document.getDocumentKey().toJson());

                    Document fullDocument = collection.find(eq("_id", newDoc.get("_id"))).first();


                        if (fullDocument != null) {
                            String json = fullDocument.toJson();
                            ProducerRecord<String, String> record = new ProducerRecord<>(KAFKA_TOPIC, json);
                            producer.send(record);
                            ThreadContext.put("statusCode","200");
                            logger.info("Service MONGO - KAFKA Success:2.0 - Change Stream Doc[--] sent to Kafka");
                            ThreadContext.clearMap();

                        }


                }
            }
        }
        catch (Exception e){
            ThreadContext.put("statusCode","400");
            logger.error("Service MONGO - KAFKA Failed - An error occurred while connecting to MongoDB");
            ThreadContext.clearMap();
            throw new RuntimeException("Service MONGO - KAFKA Failed - An error occurred while connecting to MongoDB");
        }
    }
    private static Properties loadProperties(final String configFile) {
        Properties properties = new Properties();
        try (InputStream inputStream = Changedatacapture.class.getClassLoader().getResourceAsStream(configFile)) {
            properties.load(inputStream);
        } catch (IOException e) {
            ThreadContext.put("statusCode","400");
            logger.error("Service MONGO - KAFKA Failed - An error occurred while extracting secrets from .prop file");
            ThreadContext.clearMap();
            throw new RuntimeException("Service MONGO - KAFKA Failed - An error occurred while extracting secrets from .prop file");
        }
        return properties;
    }



}



